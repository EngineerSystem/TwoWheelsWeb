﻿using System;
using System.Web.Mvc;

using TwoWheelsWeb.Services;

namespace TwoWheelsWeb.Utils
{
    public class AuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            if (!TokenService.IsAuthenticated())
            {
                filterContext.HttpContext.Response.RedirectToRoute(RouteConfig.loginRouteName);
            }
        }
    }
}