﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using TwoWheelsWeb.Models;
using TwoWheelsWeb.Models.MotorcycleModels;

namespace TwoWheelsWeb.ViewModels
{
    public class MotorcycleViewModels
    {
        public class MotorcycleModuleViewModel
        {
            public string UserMotorcycleId { get; set; }
            public string Name { get; set; }
            public int CurrentMileage { get; set; }
            public string Mark { get; set; }
            public string Model { get; set; }
            public string Type { get; set; }
            public int ProductionYear { get; set; }
        }

        public class MotorcycleDetailsViewModel
        {
            public string UserMotorcycleId { get; set; }
            public string Name { get; set; }
            public int CurrentMileage { get; set; }
            public string Mark { get; set; }
            public string Model { get; set; }
            public string Type { get; set; }
            public int ProductionYear { get; set; }
            public int InitialMileage { get; set; }
            public float AverageFuelConsumption { get; set; }
            public List<OperatingPart> OperatingParts { get; set; }
        }

        public class MotorcycleModelViewModel
        {
            public MotorcycleModel MotorcycleModel { get; set; }
            public List<string> MotorcycleTypes => new List<string>
            {
                "Sport",
                "Tourer",
                "Cross",
                "Chopper"
            };
        }

        public class ManageOperatingPartsViewModel
        {
            public List<OperatingPart> OperatingParts { get; set; }
            public string UserMotorcycleId { get; set; }
        }

        public class CreateOperatingPartViewModel
        {
            public string UserMotorcycleId { get; set; }
            public OperatingPart OperatingPart { get; set; }
        }

        public class AddRefuelingViewModel
        {
            public string UserMotorcycleId { get; set; }
            public Refuel Refuel { get; set; }
        }

        public class RefuelingSummaryViewModel
        {
            public string UserMotorcycleId { get; set; }
            public float AverageFuelConsumption { get; set; }
        }

        public class ChangePartViewModel
        {
            public string OperatingPartId { get; set; }
            public string UserMotorcycleId { get; set; }
            public string OperatingPartName { get; set; }
            public int CurrentMileage { get; set; }
        }
    }
}