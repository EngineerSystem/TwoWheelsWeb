﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

using TwoWheelsWeb.Models.ForumModels;

namespace TwoWheelsWeb.ViewModels
{
    public class ForumViewModels
    {
        public class ForumMainPageViewModel
        {
            public List<CategoryWithSubcategories> List { get; set; }
        }

        public class CategoryWithSubcategories
        {
            public Category Category { get; set; }
            public List<SubCategory> SubCategories { get; set; }
        }

        public class CategoryTopicsViewModel
        {
            public List<Topic> Topics { get; set; }
            public List<SubCategory> SubCategories { get; set; }
            public string CategoryId { get; set; }
            public string CategoryName { get; set; }
        }

        public class CreateTopicViewModel
        {
            public string CategoryId { get; set; }
            public Topic Topic { get; set; }
            [AllowHtml]
            public string PostContent { get; set; }
            public string Tags { get; set; }
        }

        public class TopicContentViewModel
        {
            public string Username { get; set; }
            public Topic Topic { get; set; }
            public List<PostWithAuthor> Posts { get; set; }
        }

        public class PostWithAuthor
        {
            public Post Post { get; set; }
            public string Username { get; set; }
        }

        public class CreatePostViewModel
        {
            public Post Post { get; set; }
            public string TopicId { get; set; }
        }

        public class SearchedTopicsViewModel
        {
            public string SearchedTag { get; set; }
            public List<Topic> Topics { get; set; }
        }

        public class CreateCategoryViewModel
        {
            public Category Category { get; set; }
            public string Id { get; set; }
        }
    }
}