﻿using System.Collections.Generic;
using TwoWheelsWeb.Models.ReviewModels;

namespace TwoWheelsWeb.ViewModels
{
    public class ReviewViewModels
    {
        public class ObjectsToReviewViewModel
        {
            public List<ObjectToReview> Objects { get; set; }
        }

        public class ObjectToReview
        {
            public string ObjectId { get; set; }
            public string Mark { get; set; }
            public string Model { get; set; }
            public GearCategory GearCategory { get; set; }
        }

        public class CreateGearViewModel
        {
            public Gear Gear { get; set; }
            public List<GearCategory> GearCategories { get; set; }
        }

        public class ReviewMainViewModel
        {
            public List<ReviewMain> Reviews { get; set; }
        }

        public class ReviewMain
        {
            public string Mark { get; set; }
            public string Model { get; set; }
            public string ObjectId { get; set; }
            public Review Review { get; set; }
        }

        public class CreateReviewViewModel
        {
            public Review Review { get; set; }
            public string ObjectId { get; set; }
            public string Name { get; set; }
        }

        public class ObjectReviewsViewModel
        {
            public float Average { get; set; }
            public List<Review> Reviews { get; set; }
            public string ObjectId { get; set; }
            public string Name { get; set; }
        }
    }
}