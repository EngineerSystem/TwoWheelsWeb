﻿using System.Collections.Generic;
using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ForumModels
{
    public class Category : IEntityBase
    {
        public string ObjectId { get; set; }
        public string Name { get; set; }

        public List<string> Topics { get; set; }
        public List<string> SubCategories { get; set; }
    }
}