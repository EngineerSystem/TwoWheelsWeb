﻿using System;
using System.Web.Mvc;

using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ForumModels
{
    public class Post : IEntityBase
    {
        public string ObjectId { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public DateTime PostDate { get; set; }
        public DateTime EditDate { get; set; }
        public bool IsEdited { get; set; }

        public string UserId { get; set; }
    }
}