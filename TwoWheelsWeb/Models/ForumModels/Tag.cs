﻿using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ForumModels
{
    public class Tag
    {
        public string Name { get; set; }
    }
}