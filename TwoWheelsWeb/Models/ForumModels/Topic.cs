﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ForumModels
{
    public class Topic : IEntityBase
    {
        public string ObjectId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public string UserId { get; set; }
        public List<string> Posts { get; set; }
        public List<Tag> Tags { get; set; }
    }
}