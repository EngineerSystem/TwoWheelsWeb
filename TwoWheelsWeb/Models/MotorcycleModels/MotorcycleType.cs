namespace TwoWheelsWeb.Models.MotorcycleModels
{
    public enum MotorcycleType
    {
        None,
        Sport,
        Tourer,
        Cross,
        Chopper
    }
}