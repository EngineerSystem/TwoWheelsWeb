﻿using System.Collections.Generic;

namespace TwoWheelsWeb.Models.MotorcycleModels
{
    public class UserMotorcycle
    {
        public string ObjectId { get; set; }
        public string UserId { get; set; }
        public string MotorcycleModelId { get; set; }
        public string Name { get; set; }
        public int InitialMileage { get; set; }
        public int CurrentMileage { get; set; }
        public float LastTankFuelConsumption { get; set; }
        public float AverageFuelConsumption { get; set; }
        public List<OperatingPart> OperatingParts { get; set; }
    }
}