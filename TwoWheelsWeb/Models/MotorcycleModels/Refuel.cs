﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TwoWheelsWeb.Models.MotorcycleModels
{
    public class Refuel
    { 
        public string ObjectId { get; set; }
        public float FuelUnitPrice { get; set; }
        public float RefuelingPrice { get; set; }
        public float FuelCapacity { get; set; }
        public int CurrentMileage { get; set; }
        public float LastFuelConsumption { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
    }
}