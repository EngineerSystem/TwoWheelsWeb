﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.MotorcycleModels
{
    public class MotorcycleModel : IEntityBase, IReviewable
    {
        public string ObjectId { get; set; }
        
        public string FullName => $"{this.Mark} {this.Model}";

        public string Mark { get; set; }

        [DisplayName("Model with capacity")]
        public string Model { get; set; }
        
        [DisplayName("Motorcycle type")]
        public string Type { get; set; }

        [DisplayName("Year of production")]
        public int ProductionYear { get; set; }

        public List<string> Reviews { get; set; }
    }
}