﻿using System;

namespace TwoWheelsWeb.Models.MotorcycleModels
{
    public class OperatingPart
    {
        public string ObjectId { get; set; }
        public string Name { get; set; }
        public int LifeSpanInKilometers { get; set; }
        public int LastChange { get; set; }
        public float UsePercentage { get; set; }

        public OperatingPart()
        {
            
        }

        #region StandardValuesConstructors

        public OperatingPart(string name, MotorcycleType type)
        {
            switch (name.ToLower().Trim())
            {
                case "engineoil":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            this.LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Tourer:
                            this.LifeSpanInKilometers = 6000;
                            break;
                        case MotorcycleType.Cross:
                            this.LifeSpanInKilometers = 3000;
                            break;
                        case MotorcycleType.Chopper:
                            this.LifeSpanInKilometers = 6000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
                case "chain":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            this.LifeSpanInKilometers = 10000;
                            break;
                        case MotorcycleType.Tourer:
                            this.LifeSpanInKilometers = 15000;
                            break;
                        case MotorcycleType.Cross:
                            this.LifeSpanInKilometers = 8000;
                            break;
                        case MotorcycleType.Chopper:
                            this.LifeSpanInKilometers = 15000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
                case "tires":
                    switch (type)
                    {
                        case MotorcycleType.Sport:
                            this.LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Tourer:
                            this.LifeSpanInKilometers = 10000;
                            break;
                        case MotorcycleType.Cross:
                            this.LifeSpanInKilometers = 5000;
                            break;
                        case MotorcycleType.Chopper:
                            this.LifeSpanInKilometers = 10000;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(type), type, null);
                    }
                    break;
            }
        }

        public OperatingPart(string name, MotorcycleType type, int lastChange) : this(name, type)
        {
            this.LastChange = lastChange;
        }

        public OperatingPart(string name, MotorcycleType type, float usePercentage) : this(name, type)
        {
            this.UsePercentage = usePercentage;
        }

        #endregion

        #region UserDefinedPartsContructors

        public OperatingPart(string name, int lifeSpanInKilometers, int lastChange)
        {
            this.Name = name;
            this.LifeSpanInKilometers = lifeSpanInKilometers;
            this.LastChange = lastChange;
        }

        public OperatingPart(string name, int lifeSpanInKilometers, float usePercentage)
        {
            this.Name = name;
            this.LifeSpanInKilometers = lifeSpanInKilometers;
            this.UsePercentage = usePercentage;
        }

        #endregion
    }
}