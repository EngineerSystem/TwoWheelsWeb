﻿using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ReviewModels
{
    public class GearCategory : IEntityBase
    {
        public string ObjectId { get; set; }
        public string Category { get; set; }
    }
}