﻿using System;
using System.ComponentModel.DataAnnotations;
using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ReviewModels
{
    public class Review : IEntityBase
    {
        public string ObjectId { get; set; }
        [Range(1, 6)]
        public float Stars { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}