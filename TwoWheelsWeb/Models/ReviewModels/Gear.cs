﻿using System.Collections.Generic;

using TwoWheelsWeb.Interfaces;

namespace TwoWheelsWeb.Models.ReviewModels
{
    public class Gear : IEntityBase, IReviewable
    {
        public string ObjectId { get; set; }
        public string Mark { get; set; }
        public string Model { get; set; }
        public string GearCategory { get; set; }
        public List<string> Reviews { get; set; }
    }
}