﻿using System;
using Newtonsoft.Json;

namespace TwoWheelsWeb.Models.AccountModels
{
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("userName")]
        public string UserName { get; set; }
        [JsonProperty("userId")]
        public string UserId { get; set; }
        [JsonProperty(".issued")]
        public DateTime Issued { get; set; }
        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
    }
}