﻿using System.Threading.Tasks;
using System.Web.Mvc;

using TwoWheelsWeb.Models.ForumModels;
using TwoWheelsWeb.Services;

using static TwoWheelsWeb.ViewModels.ForumViewModels;

namespace TwoWheelsWeb.Controllers
{
    public class ForumController : Controller
    {
        private readonly ForumService _forumService;

        public ForumController()
        {
            this._forumService = new ForumService();
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var model = await _forumService.GetForumMainPageViewModel();

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> GetCategoryTopics(string id)
        {
            var topicsModel = await _forumService.GetCategoryTopics(id);

            return View(topicsModel);
        }

        [HttpGet]
        public async Task<ActionResult> GetTopic(string id)
        {
            var content = await this._forumService.GetTopicContent(id);

            return this.View(content);
        }

        [HttpGet]
        public ActionResult CreateTopic(string id)
        {
            var model = this._forumService.GetCreateTopicViewModel(id);

            return this.View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateTopic(CreateTopicViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var response = await this._forumService.CreateTopic(model);

            return !response.Equals(string.Empty)
                ? this.RedirectToAction("GetTopic", new { id = response.Trim('"') })
                : this.RedirectToAction("CreateTopic", new { id = model.CategoryId });
        }

        [HttpGet]
        public ActionResult CreatePost(string id)
        {
            var model = this._forumService.GetCreatePostViewModel(id);
            
            return this.View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreatePost(CreatePostViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var isCreated = await this._forumService.CreatePost(model);

            return isCreated ? RedirectToAction("GetTopic", new { id = model.TopicId }) : RedirectToAction("CreatePost", new {id=model.TopicId});
        }

        [HttpGet]
        public ActionResult CreateCategory(string id = null)
        {
            var model = this._forumService.GetCreateCategoryViewModel(id);

            return this.View(model);
        }

        [HttpPost]
        public async Task<ActionResult> CreateCategory(CreateCategoryViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = await this._forumService.CreateCategory(model);

            return result ? RedirectToAction("Index") : this.RedirectToAction("CreateCategory");
        }

        [HttpGet]
        public async Task<ActionResult> SearchByTag(string tag)
        {
            var model = await this._forumService.SearchByTag(tag);

            return this.View(model);
        }
    }

}