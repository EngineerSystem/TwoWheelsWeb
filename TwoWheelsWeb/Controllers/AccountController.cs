﻿using System.Threading.Tasks;
using System.Web.Mvc;

using TwoWheelsWeb.Interfaces;
using TwoWheelsWeb.Services;
using TwoWheelsWeb.ViewModels;

namespace TwoWheelsWeb.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(this.accountService.GetLoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(AccountViewModels.LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Login");
            }

            var loginResult = await this.accountService.Login(model);

            return loginResult ? RedirectToAction("Index", "Home") : RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public async Task<ActionResult> Logout()
        {
            await this.accountService.Logout();

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Register()
        {
            return this.View(this.accountService.GetRegisterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(AccountViewModels.RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Register");
            }

            var registerResult = await this.accountService.Register(model);

            return RedirectToAction(registerResult ? "Login" : "Register");
        }
    }
}