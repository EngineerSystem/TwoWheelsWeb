﻿using System.Threading.Tasks;
using System.Web.Mvc;
using TwoWheelsWeb.Models.ReviewModels;
using TwoWheelsWeb.Services;
using static TwoWheelsWeb.ViewModels.ReviewViewModels;

namespace TwoWheelsWeb.Controllers
{
    public class ReviewController : Controller
    {
        private readonly ReviewService _reviewService;

        public ReviewController()
        {
            this._reviewService = new ReviewService();
        }

        [HttpGet]
        public async Task<ActionResult> GetObjectsToReview()
        {
            var result = await _reviewService.GetObjectsToReview();

            return PartialView(result);
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            var model = await _reviewService.GetModelForIndexPage();

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> CreateGear(string id)
        {
            return this.View(await _reviewService.GetCreateGearViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> CreateGear(CreateGearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = await _reviewService.CreateGear(model);

            return response ? RedirectToAction("GetObjectsToReview") : RedirectToAction("CreateGear");
        }

        [HttpGet]
        public ActionResult CreateGearCategory()
        {
            return PartialView(new GearCategory());
        }

        [HttpPost]
        public async Task<ActionResult> CreateGearCategory(GearCategory model)
        {
            if (!ModelState.IsValid)
            {
                return PartialView(model);
            }

            var response = await _reviewService.CreateGearCategory(model);

            return response ? RedirectToAction("GetObjectsToReview") : RedirectToAction("CreateGearCategory");
        }

        [HttpGet]
        public ActionResult CreateReview(string id)
        {
            return this.PartialView(_reviewService.GetCreateReviewViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> CreateReview(CreateReviewViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = await _reviewService.CreateReview(model);

            return response ? RedirectToAction("GetReviews", new { id = model.ObjectId}) : RedirectToAction("CreateReview");
        }

        [HttpGet]
        public async Task<ActionResult> GetReviews(string id)
        {
            var reviews = await _reviewService.GetReviewsForObject(id);

            return View(reviews);
        }
    }
}