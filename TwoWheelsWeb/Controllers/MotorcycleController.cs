﻿using System.Threading.Tasks;
using System.Web.Mvc;

using TwoWheelsWeb.Models;
using TwoWheelsWeb.Models.MotorcycleModels;
using TwoWheelsWeb.Services;

using static TwoWheelsWeb.ViewModels.MotorcycleViewModels;

namespace TwoWheelsWeb.Controllers
{
    public class MotorcycleController : Controller
    {
        private readonly MotorcycleService _motorcycleService;

        public MotorcycleController(MotorcycleService motorcycleService)
        {
            this._motorcycleService = motorcycleService;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return View(await this._motorcycleService.GetUserMotorcycles(TokenService.UserId));
        }

        [HttpGet]
        public ActionResult RedirectToCreate(string id)
        {
            return Json(new { url = Url.Action("Create", "Motorcycle", new { id = id })}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create(string id)
        {
            return this.View(this._motorcycleService.GetCreateViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> Create(UserMotorcycle model)
        {
            if (!ModelState.IsValid)
            {
                return View(model.MotorcycleModelId);
            }
            var response = await _motorcycleService.CreateUserMotorcycle(model);

            if (response.Equals(string.Empty))
            {
                return View(model.MotorcycleModelId);
            }

            return RedirectToAction("ManageOperatingParts", new {id = response});
        }

        [HttpGet]
        public async Task<ActionResult> MotorcycleModels()
        {
            var models = await this._motorcycleService.GetMotorcycleModels();
            return this.View(models);
        }

        [HttpGet]
        public ActionResult CreateModel()
        {
            return this.View(_motorcycleService.GetCreateModelViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> CreateModel(MotorcycleModelViewModel model)
        {
            var createResult = await this._motorcycleService.CreateMotorcycleModel(model.MotorcycleModel);
            return this.RedirectToAction("MotorcycleModels");
        }

        [HttpGet]
        public async Task<ActionResult> ManageOperatingParts(string id)
        {
            return this.View(await this._motorcycleService.GetManageOperatingPartsViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> ManageOperatingParts(ManageOperatingPartsViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var createPartsResult = await this._motorcycleService.ManageOpeartingParts(model);

            return RedirectToAction("Details", new { id = model.UserMotorcycleId });
        }

        [HttpGet]
        public ActionResult CreatePart(string id)
        {
            return this.PartialView(this._motorcycleService.GetCreatePartViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> CreatePart(CreateOperatingPartViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = await this._motorcycleService.CreateOperatingPart(model);

            return RedirectToAction("ManageOperatingParts", new {id = model.UserMotorcycleId});
        }

        [HttpGet]
        public async Task<ActionResult> Details(string id)
        {
            var detailsResult = await this._motorcycleService.GetUserMotorcycleDetails(id);

            return View(detailsResult);
        }

        [HttpGet]
        public ActionResult AddRefueling(string id)
        {
            return this.View(this._motorcycleService.GetRefuelingViewModel(id));
        }

        [HttpPost]
        public async Task<ActionResult> AddRefueling(AddRefuelingViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return this.View(model);
            }

            var result = await this._motorcycleService.Refuel(model);

            return RedirectToAction("Details", new { id = model.UserMotorcycleId });
        }

        [HttpGet]
        public ActionResult RefuelingsSummary(string id, float fuel)
        {
            var model = _motorcycleService.GetRefuelingSummaryViewModel(id, fuel);

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Refuelings(string id)
        {
            var refuelings = await _motorcycleService.GetRefuelings(id);

            return PartialView(refuelings);
        }

        [HttpGet]
        public ActionResult ChangePart(string id, string name)
        {
            return View(_motorcycleService.GetChangePartViewModel(id, name));
        }

        [HttpPost]
        public async Task<ActionResult> ChangePart(ChangePartViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = await _motorcycleService.ChangePart(model);

            return RedirectToAction("Details", new {id = model.UserMotorcycleId});
        }
    }
}