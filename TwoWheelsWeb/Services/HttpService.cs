﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TwoWheelsWeb.Models.AccountModels;
using TwoWheelsWeb.Utils;

namespace TwoWheelsWeb.Services
{
    public class HttpService
    {
        private readonly HttpClient _client;
        public Token Token { get; set; }
        private string _userName = string.Empty;
        private string _password = string.Empty;

        public HttpService()
        {
            _client = new HttpClient { BaseAddress = new Uri(AppConfiguration.ApiBaseUri) };
        }

        public async Task<HttpResponseMessage> GetToken(string userName, string password)
        {
            if (userName.Equals(string.Empty) || password.Equals(string.Empty))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            _userName = userName;
            _password = password;

            return await GetToken();
        }

        public async Task<HttpResponseMessage> GetToken()
        {
            var credentials = new Dictionary<string, string>
            {
                { "grant_type", "password" },
                { "username", _userName },
                { "password", _password },
            };

            var response = await PostForToken(credentials);

            if (response.StatusCode != HttpStatusCode.OK) return new HttpResponseMessage(HttpStatusCode.Unauthorized);

            ParseResponse(response);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        private async Task<HttpResponseMessage> PostForToken(Dictionary<string, string> credentials)
        {
            return await _client.PostAsync("Token", new FormUrlEncodedContent(credentials));
        }

        public async Task RefreshAccessToken()
        {
            if (Token == null || Token.Expires.Subtract(DateTime.Now).Minutes < 60)
            {
                return;
            }

            var credentials = new Dictionary<string, string>
            {
                { "grant_type", "refresh_token" },
                { "refresh_token", this.Token.RefreshToken }
            };

            var response = await Post("Token", credentials);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                ParseResponse(response);
            }
        }

        public async Task<HttpResponseMessage> Get(string url, string id = null)
        {
            await RefreshAccessToken();

            if (id != null)
            {
                return await _client.GetAsync(url + "/" + id);
            }

            return await _client.GetAsync(url);
        }

        public async Task<HttpResponseMessage> GetWithAuthorize(string url, string id = null)
        {
            await RefreshAccessToken();

            AddAuthHeader();

            HttpResponseMessage response;

            if (id != null)
            {
                response = await _client.GetAsync(url + "/" + id);
            }
            else
            {
                response = await _client.GetAsync(url);
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                await this.GetToken();

                if (id != null)
                {
                    response = await _client.GetAsync(url + "/" + id);
                }
                else
                {
                    response = await _client.GetAsync(url);
                }
            }

            return response;
        }

        public async Task<HttpResponseMessage> Get(string url, string id, string detail)
        {
            await RefreshAccessToken();
            AddAuthHeader();

            var response = await _client.GetAsync(url + "/" + id + "/" + detail);

            return response;
        }

        public async Task<HttpResponseMessage> Post<T>(string url, T entity)
        {
            await RefreshAccessToken();

            var response = await _client.PostAsJsonAsync(url, entity);

            if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                response = await PostWithAuthorize(url, entity);
            }

            return response;
        }

        public async Task<HttpResponseMessage> PostWithAuthorize<T>(string url, T entity)
        {
            await RefreshAccessToken();

            if (Token == null || Token.AccessToken.Equals(string.Empty))
            {
                await GetToken();
            }

            AddAuthHeader();

            return await _client.PostAsJsonAsync(url, entity);
        }

        public async Task<HttpResponseMessage> Put<T>(string url, string id, T entity)
        {
            await RefreshAccessToken();

            var response = await _client.PutAsJsonAsync(url + "?id=" + id, entity);

            if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                response = await PutWithAuthorize(url, id, entity);
            }

            return response;
        }

        public async Task<HttpResponseMessage> PutWithAuthorize<T>(string url, string id, T entity)
        {
            await RefreshAccessToken();

            if (Token.AccessToken.Equals(string.Empty))
            {
                await GetToken();
            }

            AddAuthHeader();

            return await _client.PutAsJsonAsync(url + "?id=" + id, entity);
        }

        public async Task<HttpResponseMessage> Delete(string url, string id)
        {
            await RefreshAccessToken();

            var response = await _client.DeleteAsync(url + "?id=" + id);

            if (response.StatusCode.Equals(HttpStatusCode.Unauthorized))
            {
                response = await DeleteWithAuthorize(url, id);
            }

            return response;
        }

        public async Task<HttpResponseMessage> DeleteWithAuthorize(string url, string id)
        {
            await RefreshAccessToken();

            if (Token.AccessToken.Equals(string.Empty))
            {
                await GetToken();
            }

            AddAuthHeader();

            return await _client.DeleteAsync(url + "?id=" + id);
        }

        private void AddAuthHeader()
        {
            try
            {
                _client.DefaultRequestHeaders.Clear();
                _client.DefaultRequestHeaders.Add("Authorization", "bearer " + Token.AccessToken);
            }
            catch (Exception e)
            {
            }
        }

        private void ParseResponse(HttpResponseMessage message)
        {
            var responseString = message.Content.ReadAsStringAsync().Result;
            var responseAsJson = JsonConvert.DeserializeObject<Token>(responseString);

            Token = new Token
            {
                AccessToken = responseAsJson.AccessToken,
                RefreshToken = responseAsJson.RefreshToken,
                Expires = responseAsJson.Expires,
                ExpiresIn = responseAsJson.ExpiresIn,
                Issued = responseAsJson.Issued,
                TokenType = responseAsJson.TokenType,
                UserName = responseAsJson.UserName,
                UserId = responseAsJson.UserId
            };
        }
    }
}