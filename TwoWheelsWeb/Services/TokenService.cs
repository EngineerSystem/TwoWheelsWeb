﻿using System;
using System.Web;
using System.Web.ModelBinding;

using TwoWheelsWeb.Models.AccountModels;

namespace TwoWheelsWeb.Services
{
    public class TokenService
    {
        public TokenService() { }

        public TokenService(Token token)
        {
            AccessToken = token.AccessToken;
            RefreshToken = token.RefreshToken;
            TokenType = token.TokenType;
            UserName = token.UserName;
            UserId = token.UserId;
            Issued = token.Issued;
            Expires = token.Expires;
            ExpiresIn = token.ExpiresIn;

            ApiToken = token;
        }

        public static string AccessToken { get; set; }
        public static string RefreshToken { get; set; }
        public static string TokenType { get; set; }
        public static string UserName { get; set; }
        public static string UserId { get; set; }
        public static DateTime Issued { get; set; }
        public static DateTime Expires { get; set; }
        public static int ExpiresIn { get; set; }

        private static HttpContextBase Current => new HttpContextWrapper(HttpContext.Current);

        public static Token ApiToken
        {
            get => Current.Session["UserName"] as Token;
            set => Current.Session["UserName"] = value;
        }

        public static bool IsAuthenticated()
        {
            return !(TokenService.Expires.Equals(default(DateTime)) ||
                     DateTime.Compare(DateTime.Now, TokenService.Expires) > 0);
        }
    }

}