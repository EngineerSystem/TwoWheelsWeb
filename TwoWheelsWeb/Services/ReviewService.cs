﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TwoWheelsWeb.Models.ReviewModels;
using static TwoWheelsWeb.ViewModels.ReviewViewModels;

namespace TwoWheelsWeb.Services
{
    public class ReviewService
    {
        private readonly HttpService _httpService;

        public ReviewService()
        {
            this._httpService = new HttpService();
        }

        public async Task<ObjectsToReviewViewModel> GetObjectsToReview()
        {
            var objects = await _httpService.Get("/api/ReviewModule/ObjectsToReview");

            return await (objects.IsSuccessStatusCode ? objects.Content.ReadAsAsync<ObjectsToReviewViewModel>() : null);
        }


        public async Task<ObjectReviewsViewModel> GetReviewsForObject(string id)
        {
            var reviews = await _httpService.Get("/api/ReviewModule/ObjectReviews", id);

            return reviews.IsSuccessStatusCode
                ? await reviews
                    .Content.ReadAsAsync<ObjectReviewsViewModel>()
                : null;
        }

        public async Task<ReviewMainViewModel> GetModelForIndexPage()
        {
            var model = await _httpService.Get("/api/ReviewModule/IndexPage");

            return await (model.IsSuccessStatusCode ? model.Content.ReadAsAsync<ReviewMainViewModel>() : null);
        }

        public async Task<CreateGearViewModel> GetCreateGearViewModel()
        {
            var model = await _httpService.GetWithAuthorize("/api/ReviewModule/CreateGear");

            return await (model.IsSuccessStatusCode ? model.Content.ReadAsAsync<CreateGearViewModel>() : null);
        }

        public async Task<bool> CreateGear(CreateGearViewModel model)
        {
            model.Gear.GearCategory = await GetGearCategoryId(model.Gear.GearCategory);

            var response = await _httpService.PostWithAuthorize("/api/Gear", model.Gear);

            return response.IsSuccessStatusCode;
        }

        public async Task<string> GetGearCategoryId(string category)
        {
            var response = await _httpService.Get("/api/ReviewModule/CategoryId", category);

            return await (response.IsSuccessStatusCode ? response.Content.ReadAsStringAsync() : null);
        }

        public async Task<bool> CreateGearCategory(GearCategory model)
        {
            var response = await _httpService.PostWithAuthorize("/api/GearCategory", model);

            return response.IsSuccessStatusCode;
        }

        public CreateReviewViewModel GetCreateReviewViewModel(string reviewedObjectId)
        {
            return new CreateReviewViewModel
            {
                ObjectId = reviewedObjectId
            };
        }

        public async Task<bool> CreateReview(CreateReviewViewModel model)
        {
            var response = await _httpService.PostWithAuthorize("/api/ReviewModule/CreateReview", model);

            return response.IsSuccessStatusCode;
        }
    }
}