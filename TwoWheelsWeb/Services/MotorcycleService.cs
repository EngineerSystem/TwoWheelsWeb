﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using TwoWheelsWeb.Models.MotorcycleModels;

using static TwoWheelsWeb.ViewModels.MotorcycleViewModels;

namespace TwoWheelsWeb.Services
{
    public class MotorcycleService
    {
        private readonly HttpService _httpService;

        public MotorcycleService(HttpService httpService)
        {
            this._httpService = httpService;
        }

        public async Task<List<MotorcycleModuleViewModel>> GetUserMotorcycles(string userId)
        {
            var motorcyclesList = await this._httpService.GetWithAuthorize("/api/MotorcycleModule/Get", userId);

            return !motorcyclesList.IsSuccessStatusCode ?
                null : motorcyclesList.Content.ReadAsAsync<List<MotorcycleModuleViewModel>>().Result;
        }

        public UserMotorcycle GetCreateViewModel(string modelId)
        {
            return new UserMotorcycle
            {
                MotorcycleModelId = modelId
            };
        }

        public async Task<List<MotorcycleModel>> GetMotorcycleModels()
        {
            var modelsResponse = await this._httpService.GetWithAuthorize("/api/MotorcycleModel");
            var model = modelsResponse.Content.ReadAsAsync<List<MotorcycleModel>>();

            return model.Result;
        }

        public MotorcycleModelViewModel GetCreateModelViewModel()
        {
            return new MotorcycleModelViewModel
            {
                MotorcycleModel = new MotorcycleModel()
            };
        }

        public async Task<string> GetUserMotorcycleType(string userMotorcycleId)
        {
            var typeResponse = await this._httpService.GetWithAuthorize("/api/MotorcycleModule/GetType", userMotorcycleId);

            return typeResponse.Content.ToString();
        }

        public async Task<bool> CreateMotorcycleModel(MotorcycleModel model)
        {
            var createResponse = await this._httpService.PostWithAuthorize("/api/MotorcycleModel", model);
            return createResponse.IsSuccessStatusCode;
        }

        public async Task<MotorcycleDetailsViewModel> GetUserMotorcycleDetails(string id)
        {
            var detailsResponse = await _httpService.GetWithAuthorize("/api/MotorcycleModule/GetDetails", id);
            var response = detailsResponse.Content.ReadAsAsync<MotorcycleDetailsViewModel>().Result;

            return detailsResponse.IsSuccessStatusCode ? response : null;
        }

        public async Task<string> CreateUserMotorcycle(UserMotorcycle model)
        {
            model.UserId = TokenService.UserId;
            model.CurrentMileage = model.InitialMileage;
            var createResponse = await _httpService.PostWithAuthorize("/api/MotorcycleModule/CreateUserMotorcycle", model);

            return createResponse.IsSuccessStatusCode ? 
                 createResponse.Content.ReadAsAsync<UserMotorcycle>().Result.ObjectId : string.Empty;
        }

        public async Task<ManageOperatingPartsViewModel> GetManageOperatingPartsViewModel(string userMotorcycleId)
        {
            var parts = await _httpService.GetWithAuthorize("/api/MotorcycleModule/GetParts", userMotorcycleId);
            List<OperatingPart> response = new List<OperatingPart>();

            if (parts.IsSuccessStatusCode)
            {
                response = await parts.Content.ReadAsAsync<List<OperatingPart>>();
            }
            
            return new ManageOperatingPartsViewModel
            {
                UserMotorcycleId = userMotorcycleId,
                OperatingParts = parts.IsSuccessStatusCode ? response : null,
            };
        }

        public async Task<bool> ManageOpeartingParts(ManageOperatingPartsViewModel model)
        {
            var createPartsResponse = await this._httpService.PostWithAuthorize("/api/MotorcycleModule/UpdateParts", model);

            return createPartsResponse.IsSuccessStatusCode;
        }

        public async Task<bool> CreateOperatingPart(CreateOperatingPartViewModel model)
        {
            var createResult = await this._httpService.PostWithAuthorize("/api/MotorcycleModule/CreatePart", model);

            return createResult.IsSuccessStatusCode;
        }

        public CreateOperatingPartViewModel GetCreatePartViewModel(string id)
        {
            return new CreateOperatingPartViewModel
            {
                UserMotorcycleId = id,
                OperatingPart = new OperatingPart()
            };
        }

        public AddRefuelingViewModel GetRefuelingViewModel(string id)
        {
            return new AddRefuelingViewModel { UserMotorcycleId = id };
        }

        public async Task<bool> Refuel(AddRefuelingViewModel model)
        {
            var refuelResult = await this._httpService.PostWithAuthorize("/api/MotorcycleModule/Refuel", model);
            //TODO 
            return refuelResult.IsSuccessStatusCode;
        }

        public async Task<List<Refuel>> GetRefuelings(string userMotorcycleId)
        {
            var refuelings = await _httpService.GetWithAuthorize("/api/MotorcycleModule/GetRefuelings", userMotorcycleId);

            return refuelings.IsSuccessStatusCode ? refuelings.Content.ReadAsAsync<List<Refuel>>().Result : null;
        }

        public RefuelingSummaryViewModel GetRefuelingSummaryViewModel(string userMotorcycleId, float averageFuelConsumption)
        {
            return new RefuelingSummaryViewModel
            {
                UserMotorcycleId = userMotorcycleId,
                AverageFuelConsumption = averageFuelConsumption
            };
        }

        public ChangePartViewModel GetChangePartViewModel(string userMotorcycleId, string name)
        {
            return new ChangePartViewModel
            {
                UserMotorcycleId = userMotorcycleId,
                OperatingPartName = name
            };
        }

        public async Task<bool> ChangePart(ChangePartViewModel model)
        {
            var response = await _httpService.PostWithAuthorize("/api/MotorcycleModule/ChangePart", model);

            return response.IsSuccessStatusCode;
        }
    }
}