﻿using System.Threading.Tasks;

using TwoWheelsWeb.Interfaces;

using static TwoWheelsWeb.ViewModels.AccountViewModels;

namespace TwoWheelsWeb.Services
{
    public class AccountService : IAccountService
    {
        private readonly HttpService httpService;

        public AccountService(HttpService httpService)
        {
            this.httpService = httpService;
        }

        public LoginViewModel GetLoginViewModel()
        {
            return new LoginViewModel();
        }

        public RegisterViewModel GetRegisterViewModel()
        {
            return new RegisterViewModel();
        }

        public async Task<bool> Login(LoginViewModel model)
        {
            var response = await this.httpService.GetToken(model.Email, model.Password);

            if (response.IsSuccessStatusCode)
            {
                var tokenService = new TokenService(this.httpService.Token);
            }

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> Register(RegisterViewModel model)
        {
            var response = await this.httpService.Post("/api/Account/Register", model);

            return response.IsSuccessStatusCode;
        }

        public async Task Logout()
        {
            await this.httpService.PostWithAuthorize("/api/Account/Logout", new {});
        }
    }
}