﻿using System.Net.Http;
using System.Threading.Tasks;

using TwoWheelsWeb.Models.ForumModels;

using static TwoWheelsWeb.ViewModels.ForumViewModels;

namespace TwoWheelsWeb.Services
{
    public class ForumService
    {
        private readonly HttpService _httpService;

        public ForumService()
        {
            this._httpService = new HttpService();
        }
        
        public async Task<ForumMainPageViewModel> GetForumMainPageViewModel()
        {
            var model = await _httpService.Get("/api/ForumModule/Index");

            return await (model.IsSuccessStatusCode ? model.Content.ReadAsAsync<ForumMainPageViewModel>() : null);
        }

        public CreateTopicViewModel GetCreateTopicViewModel(string categoryId)
        {
            return new CreateTopicViewModel
            {
                CategoryId = categoryId
            };
        }

        public async Task<TopicContentViewModel> GetTopicContent(string topicId)
        {
            var content = await this._httpService.Get("/api/ForumModule/GetTopicContent", topicId);

            return await (content.IsSuccessStatusCode ? content.Content.ReadAsAsync<TopicContentViewModel>() : null);
        }

        public async Task<string> CreateTopic(CreateTopicViewModel model)
        {
            model.Topic.UserId = TokenService.UserId;
            var response = await this._httpService.PostWithAuthorize("/api/ForumModule/CreateTopic", model);

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<CategoryTopicsViewModel> GetCategoryTopics(string categoryId)
        {
            var topics = await _httpService.Get("/api/ForumModule/GetCategoryTopics", categoryId);
            
            return await (topics.IsSuccessStatusCode ? topics.Content.ReadAsAsync<CategoryTopicsViewModel>() : null);
        }

        public CreatePostViewModel GetCreatePostViewModel(string topicId)
        {
            return new CreatePostViewModel
            {
                TopicId = topicId,
            };
        }

        public async Task<bool> CreatePost(CreatePostViewModel model)
        {
            model.Post.UserId = TokenService.UserId;
            var response = await this._httpService.PostWithAuthorize("/api/ForumModule/CreatePost", model);

            return response.IsSuccessStatusCode;
        }

        public async Task<SearchedTopicsViewModel> SearchByTag(string tag)
        {
            var topics = await this._httpService.Get("/api/ForumModule/SearchByTag", tag);

            return await (topics.IsSuccessStatusCode ? topics.Content.ReadAsAsync<SearchedTopicsViewModel>() : null);
        }

        public CreateCategoryViewModel GetCreateCategoryViewModel(string id = null)
        {
            return id == null ? new CreateCategoryViewModel {Id = id} : new CreateCategoryViewModel();
        }

        public async Task<bool> CreateCategory(CreateCategoryViewModel model)
        {
            var result = model.Id == null
                ? await this._httpService.Post("/api/ForumModule/CreateCategory", model.Category)
                : await this._httpService.Post($"/api/ForumModule/CreateCategory/{model.Id}", model.Category);

            return result.IsSuccessStatusCode;
        }
    }
}