using System.Collections.Generic;

namespace TwoWheelsWeb.Interfaces
{
    public interface IReviewable
    {
        List<string> Reviews { get; set; }
    }
}