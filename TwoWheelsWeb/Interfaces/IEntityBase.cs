﻿namespace TwoWheelsWeb.Interfaces
{
    public interface IEntityBase
    {
        string ObjectId { get; set; }
    }
}