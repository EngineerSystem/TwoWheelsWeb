﻿using System.Threading.Tasks;

using TwoWheelsWeb.ViewModels;

namespace TwoWheelsWeb.Interfaces
{
    public interface IAccountService
    {
        AccountViewModels.LoginViewModel GetLoginViewModel();
        AccountViewModels.RegisterViewModel GetRegisterViewModel();
        Task<bool> Login(AccountViewModels.LoginViewModel model);
        Task<bool> Register(AccountViewModels.RegisterViewModel model);

        Task Logout();
    }
}