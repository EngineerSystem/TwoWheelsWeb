﻿using System.Web.Mvc;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using TwoWheelsWeb.Interfaces;
using TwoWheelsWeb.Services;

namespace TwoWheelsWeb.Windsor
{
    public class WindsorInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
            .BasedOn<IController>()
            .LifestylePerWebRequest());

            container.Register(Classes.FromAssemblyContaining<IEntityBase>().BasedOn(typeof(IEntityBase)));

            container.Register(Component.For<MotorcycleService>().ImplementedBy<MotorcycleService>());
            container.Register(Component.For<HttpService>().ImplementedBy<HttpService>());
            container.Register(Component.For<IAccountService>().ImplementedBy<AccountService>());


            container.Register(Classes.FromThisAssembly().InNamespace("TwoWheelsWeb.ViewModels"));
        }
    }
}